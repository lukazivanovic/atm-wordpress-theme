<?php
/**
 * Load all files recursive from inc dir
 */
function batchAutoload() {
    $iteratorObject = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/../inc/'));
    $files = new RegexIterator($iteratorObject, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);
    
    foreach($files as $file) {
        $fileName = reset($file);
        
        include $fileName;
    }
}

batchAutoload();


<?php

declare(strict_types = 1);

namespace app\helpers;

use WP_Term;

class PostHelper
{

    private $postID;

    const IMAGE_SIZE = 'url';

    /**
     * CategoryHelper constructor.
     * @param $postID
     */
    public function __construct($postID)
    {
        $this->postID = $postID;
    }

    /**
     * @return bool|string
     */
    public function getCategoryString()
    {
        $category_string = '';

        /**
         * @var $category_list WP_Term[]
         */
        $category_list = get_the_category($this->postID);

        if (is_array($category_list)) {
            foreach ($category_list as $category) {
                $category_string .= sprintf(' | %s', $category->name);
            }
            $category_string = substr($category_string, 3);
        }

        return $category_string;
    }

    /**
     * @return mixed|string
     */
    public function getFeaturedImage()
    {
        $image_url = '';
        $image_array = wp_get_attachment_image_src(get_post_thumbnail_id($this->postID), self::IMAGE_SIZE);
        if (is_array($image_array)) {
            $image_url = $image_array[0];
        }

        return $image_url;
    }

    /**
     * @return false|string
     */
    public function getPermalink()
    {
        return get_permalink($this->postID);
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return get_the_date('D, m.Y', $this->postID);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return get_the_title($this->postID);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return get_post($this->postID)->post_content;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return get_the_excerpt($this->postID);
    }


    //treba da ga prebacim u model
    public function getPosts(string $postType = 'post', int $category_id = 0, string $category = 'category'): ?array
    {

        $query =
            [
                'post_type' => $postType,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'no_found_rows' => true,
                'fields' => 'ids',
            ];

        if ($category_id !== 0) {
            $query['tax_query'] =
             [
                [
                    'taxonomy' => $category,
                    'field' => 'term_id',
                    'terms' => $category_id,

                ],
            ];
        }

        $posts = get_posts($query);

        if (count($posts) === 0) {
            return null;
        }

        return $posts;
    }

}



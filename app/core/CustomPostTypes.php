<?php
/**
 *  Template function for adding custom post type. AutoLoader class should auto include this file.
 *
 *  Following code will add a new custom post type.
 *  Please check codex for more information on custom post types
 *  Custom post types: https://codex.wordpress.org/Post_Types#Custom_Post_Types
 *  Register post types: https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 */

namespace app\core;

class CustomPostTypes
{
    const POST_DEFAULT = 'post';
    const POST_TYPE_PROJECT = 'project';

    const TAXONOMY_PROJECT = 'project-category';

    public function register(): void
    {
        $this->register_post_types();
        $this->register_theme_options();
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function register_post_types(): void
    {
        add_action(
            'init',
            function () {

                register_post_type(
                    self::POST_TYPE_PROJECT,
                    [
                        'labels' => [
                            'name' => __('Projects'),
                            'singular_name' => __('Project'),
                        ],
                        'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
                        'public' => true,
                        'publicly_queryable' => true,
                        'has_archive' => true,
                        'hierarchical' => false,
                        'show_in_rest' => true,
                        'menu_icon' => 'dashicons-products',
                        'rewrite' => [
                            'slug' => self::POST_TYPE_PROJECT,
                            'with_front' => true,
                        ],
                    ]
                );

                register_taxonomy(
                    self::TAXONOMY_PROJECT,  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
                    self::POST_TYPE_PROJECT,        // post type name
                    [
                        'hierarchical' => true,
                        'label' => 'Project Category',  // Display name
                        'query_var' => true,
                        'show_in_rest' => true,
                        'rewrite' => [
                            'slug' => self::TAXONOMY_PROJECT, // This controls the base slug that will display before each term
                            'with_front' => false, // Don't display the category base before
                        ],
                    ]
                );
            }
        );

    }

    private function register_theme_options(): void
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(
                [
                    'page_title' => 'Theme options',
                    'menu_title' => 'Theme options',
                    'menu_slug' => 'theme-options',
                    'capability' => 'edit_posts',
                    'parent_slug' => '',
                    'position' => false,
                    'icon_url' => false,
                    'redirect' => false,
                ]
            );

            acf_add_options_sub_page(
                [
                    'page_title' => 'Archive Pages',
                    'menu_title' => 'Archive Pages',
                    'menu_slug' => 'archive-pages-options',
                    'capability' => 'edit_posts',
                    'parent_slug' => 'theme-options',
                    'position' => false,
                    'icon_url' => false,
                    'redirect' => false,
                ]
            );

            acf_add_options_sub_page(
                [
                    'page_title' => 'Header',
                    'menu_title' => 'Header',
                    'menu_slug' => 'header-options',
                    'capability' => 'edit_posts',
                    'parent_slug' => 'theme-options',
                    'position' => false,
                    'icon_url' => false,
                    'redirect' => false,
                ]
            );

            acf_add_options_sub_page(
                [
                    'page_title' => 'Footer',
                    'menu_title' => 'Footer',
                    'menu_slug' => 'footer-options',
                    'capability' => 'edit_posts',
                    'parent_slug' => 'theme-options',
                    'position' => false,
                    'icon_url' => false,
                    'redirect' => false,
                ]
            );
        }
    }
}

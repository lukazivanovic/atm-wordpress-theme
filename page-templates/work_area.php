<?php /** Template Name: Work Area Template*/

use app\common\ACFDataProvider;
use app\helpers\PostHelper;
use app\helpers\SocialSharer;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

$share_helper = new SocialSharer();

$content = $acf_instance->getField('content');
$menu_title = $acf_instance->getField('menu_title');
$share_text = $acf_instance->getField('share_text');
$share_list = $acf_instance->getField('share_list');

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>
  
  <section class="cover cover--no-overlay cover--bottom-title" style="background: url('<?= $post_helper->getFeaturedImage() ?>') no-repeat;">
    <div class="container">
      <h1><?=the_title()?></h1>
    </div>
  </section>

  <section class="work">
      <div class="container">
        <div class="left">

          <?= $content ?>
          
          <div class="share">
            <span><?=$share_text?></span>
            <ul class="share__list">
            <?php 
              if(is_array($share_list) && !empty($share_list)){
                foreach($share_list as $sl){
            ?>
              <li>
                <a href="<?= $share_helper->getLinkByShareType($sl['share_type'],get_the_permalink()) ?>">
                  <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-'.$sl['icon']['title']); ?>">
                    </use>
                  </svg>
                </a>
              </li>
                <?php }} ?>
            </ul>
          </div>
        </div>
        <aside class="right">
          <h2><?=$menu_title?></h2>
          <ul class="right__nav">
          <?php create_menu_work_areas('Work Areas Menu'); ?>
          </ul>
        </aside>
      </div>
  </section>
  
   <?php
    get_partial('general/projects-in-progress');
  ?>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php /** Template Name: Reference Template*/
the_post();

use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance();
$year = $acf_instance->getField('year');
$purchaser = $acf_instance->getField('purchaser');
$investor = $acf_instance->getField('investor');
$object = $acf_instance->getField('object');
$works = $acf_instance->getField('works');
$items = $acf_instance->getField('items');

get_header();

//HOME HEADER
get_partial('layout/header');


?>
  
  <section class="references">
      <div class="container">
        <h1><?= the_title(); ?></h1>

        <table>
          <thead>
            <tr>
              <th><?=$year?></th>
              <th colspan="2"><?=$purchaser?></th>
              <th><?=$investor?></th>
              <th><?=$object?></th>
              <th colspan="2"><?=$works?></th>
            </tr>
          </thead>

          <tbody>
          <?php if(is_array($items) && !empty($items)){
              foreach($items as $item){ ?>
            <tr>
              <td><?=$item['year']?></td>
              <td colspan="2"><?=$item['purchaser']?></td>
              <td><?=$item['investor']?></td>
              <td><?=$item['object']?></td>
              <td colspan="2"><?=$item['works']?></td>
            </tr>
            <?php }} ?>
          </tbody>
        </table>
      </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php /** Template Name: Energetics Template*/

use app\common\ACFDataProvider;
use app\helpers\PostHelper;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

<?php
get_partial('general/cover', [
    'title' => $post_helper->getTitle(),
    'image' => $post_helper->getFeaturedImage()
]);
?>

    <section class="text-image">
        <div class="container">

            <!-- Row item -->
            <div class="row">
                <?php
                $energetics_repeater = $acf_instance->getField('energetics_-_repeater');
                if (is_array($energetics_repeater) && (!empty($energetics_repeater))) {
                    foreach ($energetics_repeater as $energetics_repeater_item) {
                        ?>
                        <div class="left">
                            <p><?= $energetics_repeater_item['description'] ?></p>
                            <ul>
                                <?php foreach ($energetics_repeater_item['info'] as $repeater_info) { ?>
                                    <li>
                                        <svg class="icon">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 xlink:href="<?= bu('ui/symbol-defs.svg#icon-' . $repeater_info['svg']['title']); ?>">
                                            </use>
                                        </svg>
                                        <?= $repeater_info['text'] ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="right">
                            <img src="<?= $energetics_repeater_item['image']['url'] ?>" alt="">
                        </div>
                    <?php }
                } ?>
            </div>
            <!-- Row item -->
        </div>
    </section>

<?php
$acf_instance->setPrefix('gallery_-_');
$gallery_title = $acf_instance->getField('title');
$gallery_description = $acf_instance->getField('description');
$gallery_slider = $acf_instance->getField('slider');
?>

    <section class="gallery">
        <div class="container">
            <?php
            get_partial('general/general-title', [
                'title' => $gallery_title,
                'description' => $gallery_description
            ]);

            get_partial('general/gallery', [
                'gallery_slider' => $gallery_slider
            ]);
            ?>

        </div>
    </section>

<?php

get_partial('layout/footer', [
    'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
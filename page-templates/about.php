<?php /** Template Name: About Template*/
use app\common\ACFDataProvider;
use app\helpers\PostHelper;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);
//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

<section class="cover cover--no-overlay" style="background: url('<?= $post_helper->getFeaturedImage() ?>') no-repeat;"></section>

<section class="about">
  <div class="container">
    <h1><?= $post_helper->getTitle() ?></h1>
    <?php 
    echo $acf_instance->getField('info_-_text');

    get_partial('about/certificate');
    ?>

  </div>
</section>

<?php 
get_partial('about/about');
get_partial('about/partners');


get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
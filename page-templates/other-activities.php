<?php /** Template Name: Other Activities Template*/

use app\common\ACFDataProvider;
use app\helpers\PostHelper;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => $post_helper->getTitle(),
      'image' => $post_helper->getFeaturedImage()
    ]);
  
    $intro_text = $acf_instance->getField('intro_-_text');
    $intro_item = $acf_instance->getField('intro_-_item');

  ?>

  <section class="other-activities">
      <div class="container">
        <?=$intro_text?>
        
        <div class="other-activities__graphics">
        <?php if(is_array($intro_item)&&(!empty($intro_item))){
          foreach($intro_item as $intro_item_repeater){
         ?>
          <div>
            <svg class="icon icon--solar">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-'.$intro_item_repeater['image']['title']); ?>">
                </use>
            </svg>
            <span><?=$intro_item_repeater['title']?></span>
          </div>
        <?php }} ?>
        </div>

        <?php $about_text = $acf_instance->getField('about_-_text'); 
        echo $about_text; ?>
       
       <?php
          $feature_item = $acf_instance->getField('feature_-_item');
          if(is_array($feature_item)&&(!empty($feature_item))){ 
            foreach($feature_item as $feature_item_repeater){ 
        ?>
        <div class="feature">
          <h2><?=$feature_item_repeater['title']?></h2>
          <?php foreach($feature_item_repeater['items'] as $items){ ?>
          <div class="feature__item">
            <img src="<?= $items['image']['url'] ?>" alt="">
            <p><?=$items['text']?></p>
          </div>
          <?php } ?>
        </div>
          <?php } ?>
      </div>
          <?php } ?>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
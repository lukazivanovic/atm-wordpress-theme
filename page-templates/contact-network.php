<?php /** Template Name: Contact Network Template*/

use app\common\ACFDataProvider;
use app\helpers\PostHelper;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix('info_-_');
//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => $post_helper->getTitle(),
      'image' => $post_helper->getFeaturedImage()
    ]);
  ?>
  
  <section class="text-image">
    <div class="container">

    <?php 
      $info_text = $acf_instance->getField('text');
      $info_image = $acf_instance->getField('image'); 
    ?>
      <!-- Row item -->
      <div class="row">
        <div class="left">
        <?=$info_text?>
        </div>
        <div class="right">
          <img src="<?= $info_image['url'] ?>" alt="">
        </div>
      </div>
      <!-- Row item -->

    </div>
  </section>

  <?php
  $acf_instance->setPrefix('gallery_-_');
  $gallery_title = $acf_instance->getField('title');
  $gallery_description = $acf_instance->getField('description'); 
  $gallery_slider = $acf_instance->getField('slider'); 
?>
  <section class="gallery">
    <div class="container">
      <?php
        get_partial('general/general-title', [
          'title' => $gallery_title,
          'description' => $gallery_description
        ]);
      
      get_partial('general/gallery', [
        'gallery_slider' => $gallery_slider
      ]);
      ?>

      
    </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php /** Template Name: Home Template*/

get_header();

get_partial('layout/header');

get_partial('home/cover');

get_partial('general/projects-in-progress');

get_partial('home/work');

get_partial('home/licence');

get_partial('home/partners');

get_partial('home/certificate');

get_partial('home/fact');

get_partial('layout/footer',[
    'footerClass' => 'footer--home',
    ]);

get_footer();
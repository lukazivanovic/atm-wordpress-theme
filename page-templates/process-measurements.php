<?php /** Template Name: Process Measurements Template*/

use app\common\ACFDataProvider;
use app\helpers\PostHelper;

the_post();

$post_helper = new PostHelper(get_the_ID());
$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => $post_helper->getTitle(),
      'image' => $post_helper->getFeaturedImage()
    ]);
  
    $process = $acf_instance->getField('process');
  if(is_array($process) && !empty($process)){
  ?>
  
  <section class="text-image">
    <div class="container">

    <?php foreach($process as $item){ 

      if($item['has_image'] == 'yes'){
        get_partial('process/with_image', [
          'item' => $item
        ]);
      } else {
        get_partial('process/without_image', [
          'item' => $item
          ]);
      } 
    }    
      ?>

    </div>
  </section>

<?php
  }

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
?>
<?php /** Template Name: Contact Template*/

use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

$info = $acf_instance->getField('info');
$form_title = $acf_instance->getField('form_title');
$contactForm = $acf_instance->getOptionField('contacts_form', false);
$content = $acf_instance->getField('content');

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>
<section class="contact">
  <div class="container">
    <div class="left">
      <h1><?= the_title(); ?></h1>
      <?= $info ?>
    </div>

    <div class="right">
      <h2><?=$form_title?></h2>
      <div action="" class="contact__form">
        <?php echo do_shortcode('[contact-form-7 id="'.$contactForm->ID.'" title="'.$contactForm->post_name.'"]'); ?>
      </div>
    </div>
  </div>

  <div id="map" class="contact__map"></div>
</section>
<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php

define('SLICE_ROOT', __DIR__ . '/../');
define('PARTIALS_DIR', 'partials');
define('STATIC_DIR', 'static');

define('PROJECT_NAME', 'atm');

require_once __DIR__ . '/functions.php';

<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => 'Ostale delatnosti'
    ]);
  ?>

  <section class="other-activities">
      <div class="container">
        <h2>ENERGETSKA EFIKASNOST</h2>
        <p>Srbija danas ima najniži stepen energetske efikasnosti u Evropi. Države zapadne Evrope po kvadratnom metru potroše manje od 100 kilovat sati energije godišnje a u našoj zemlji potrošnja je od 150 do 180 kilovat sati. To je razmljivo s obzirom da je cena jednog kilovat časa u Srbiji jedna od najmanjih cena u Evropi (ne naplaćuju se razni porezi što se tiču globalnog zagrevanja, zagađivanja zemljišta, vazduha i komplet čovekove okoline). Međutim sve je manje konvencionalnih izvora energije, pa je sve usmereno ka razvijanju tehnologija koje iskorišćavaju obnovljive izvore energije (energiju Sunca, vetra, biomase i dr.).</p>
        
        <div class="other-activities__graphics">
          <div>
            <svg class="icon icon--solar">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-solar-panels'); ?>">
                </use>
            </svg>
            <span>Solarni paneli</span>
          </div>
          <div>
            <svg class="icon icon--battery">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-battery'); ?>">
                </use>
            </svg>
            <span>Baterije</span>
          </div>
          <div>
            <svg class="icon icon--sun">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-sun-energy'); ?>">
                </use>
            </svg>
            <span>Energija Sunca</span>
          </div>
        </div>

        <p>Nudimo širok spektar solarnih panela, baterija i tehničkih rešenja o korišćenju energije Sunca. Nudimo isporuku i montažu elekro opreme, kao i kompletan inženjering. Za sve dodatne informacije možete se obratiti direktno putem telefona ili elektronskom poštom.</p>
        <p>Da bi se električna energija eksploatisala na najekonomičniji način neophodno je pravilno upravljati njenom potrošnjom. S tim u vezi je ATM BG d.o.o. razvio sopstevni sistem za kompenzaciju reaktivne električne energije, kao i sistem za upravljanje potrošnjom električne energije u industriji, javnoj rasveti i dr. (detaljnije o ovom rešenju kontaktirati firmu).</p>

        <div class="feature">
          <h2>KRANOVI</h2>
          <div class="feature__item">
            <img src="<?= buStatic('images/kran.jpg'); ?>" alt="">
            <p>Inženjering, projektovanje, isporuku elektro opreme i montažu kompletnih objekata.</p>
          </div>
          <div class="feature__item">
            <img src="<?= buStatic('images/kran.jpg'); ?>" alt="">
            <p>Remont i servis postojećih objekata kao i zamenu stare elektro opreme za novu.</p>
          </div>
        </div>
      </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
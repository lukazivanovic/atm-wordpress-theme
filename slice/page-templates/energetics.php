<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => 'Procesna merenja i automatizacija'
    ]);
  ?>
  
  <section class="text-image">
    <div class="container">

      <!-- Row item -->
      <div class="row">

        <div class="left">
          <p>Izrada kompletne tehničke dokumentacije niskog, srednjeg i visokog napona, kao što su idejni, glavni i projekti izvedenog stanja, elaborati i studije za:</p>

          <ul>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Elektroenergetska postrojenja (0.4, 10, 35 i 110 kV)
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Električne instalacije i elektroenergetsku opremu
            </li>
          </ul>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/energetics-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->

      <!-- Row item -->
      <div class="row">

        <div class="left">
          <p>Izvodimo inzenjering poslove u svim oblastima energetike:</p>

          <ul>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Analizu postojećeg stanja, isporuku, ugradnju i remont elektro opreme (izrada i ugradnja elektroormana, razvodnih postrojenja)
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Nove trafo stanice od 10, 20, 35 i 110 kV, kao i imlpementaciju odgovarajuće zaštitne i upravljačke opreme u trafostanicama.
            </li>
          </ul>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/energetics-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->

      <!-- Row item -->
      <div class="row">

        <div class="left">
          <p>U okviru naših rešenja nudimo vam raznovrsnu opremu:</p>

          <ul>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Vakuumske prekidače
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Zaštita
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Upravljanje
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Ring main unit TS 20/10/0.4 kV
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Silikonske izolatore, rastavljače, odvodnici prenapona
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Srednjenaponska postrojenja
            </li>
          </ul>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/energetics-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->
    </div>
  </section>

  <section class="gallery">
    <div class="container">
      <?php
        get_partial('general/general-title', [
          'title' => 'Galerija referenci',
          'description' => 'Naš tim je stručan u gotovo svim oblastima energetike, tako da dajemo sugestije, savete i kompletna tehnička rešenja za dalekovode, trafo stanice, javnu rasvetu i dr.'
        ]);
      ?>

      <div class="gallery__wrapp">

        <a data-fancybox="gallery-1" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>BARE I KRSTIĆI</h4>
        </a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>


        <a data-fancybox="gallery-2" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>GILJE ĆUPRIJA PARAĆIN</h4>
        </a>
        <a data-fancybox="gallery-2" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-2" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>

        <a data-fancybox="gallery-3" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>VREOCI</h4>
        </a>
        <a data-fancybox="gallery-3" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>

        <a data-fancybox="gallery-4" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>KOSTOLAC</h4>
        </a>
        <a data-fancybox="gallery-4" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>

        <a data-fancybox="gallery-5" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>TO DUNAV</h4>
        </a>
        <a data-fancybox="gallery-5" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
      </div>
    </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

<section class="cover cover--no-overlay" style="background: url('<?= buStatic('images/cover-about.jpg'); ?>') no-repeat;"></section>

<section class="about">
  <div class="container">
    <h1>O nama</h1>

    <p class="about__intro">Firma ATM BG d.o.o. je nastala u postupku izdvajanja iz nekadašnje matične firme ATM Zagreb. Danas je ATM BG d.o.o. samostalna specijalizovana privatna firma sa sedištem na Novom Beogradu.</p>

    <p>ATM BG D.O.O. samostalno realizuje projekte, kao i u saradnji sa renomiranim svetskim firmama. Na zahtev Investitora možemo ponuditi i opremu sledećih proizvođača: „Siemens”, „ABB“, „Schneider Electric“, „General Electric“, „Yokogawa“, „Ohmron“, „Secheron”, „Elektroline”, „PodemCrane”, „Hoppesack”... a sve u cilju da se realizuje svaki kompletan posao iz oblasti elektrotehnike.</p>
    <p>Posedovanje licence za projektovanje i izvođenje radova, omogućava učešće u najsloženijim projektima, i daje garanciju da će svi poslovi koji budu ugovoreni, biti izvršeni na tehnički najefikasniji način, po povoljnoj ceni i u predviđenom roku. Područje rada firme:</p>

    <h3>Sertifikati</h3>
    <p>Posedovanje licence za projektovanje i izvođenje radova, omogućava učešće u najsloženijim projektima, i daje garanciju da će svi poslovi koji budu ugovoreni, biti izvršeni na tehnički najefikasniji način, po povoljnoj ceni i predviđenom roku.</p>
    
    <div class="certificates__cols">
        <div class="col">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
              </use>
          </svg>
          <span>ISO 9001</span>
        </div>
        <div class="col">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
              </use>
          </svg>
          <span>ISO 14001</span>
        </div>
        <div class="col">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
              </use>
          </svg>
          <span>OHSAS 18001</span>
        </div>
    </div>

  </div>
</section>

<section class="cover cover--no-overlay cover--inner" style="background: url('<?= buStatic('images/cover-in-about.jpg'); ?>') no-repeat;"></section>

<section class="about">
  <div class="container">
    <h3>New year clean</h3>
    <p>What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>

    <h3>Sit stand desks</h3>
    <p>Sit to stand desks are a great way of providing choice to your employees. Giving your employees the choice of whether they want to sit or stand to work, will help workplace happiness, as well as productivity and creative thinking. What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>
    <p>What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>
  
    <ul class="about__licence">
      <li>
        <div class="left">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence-blue'); ?>">
              </use>
          </svg>
          <h4>P141E1</h4>
        </div>

        <div class="right">
          <p>Licenca za projektovanje elektroenergetskih instalacija srednjeg i visokog napona za javne železničke infrastrukture sa priključcima</p>
        </div>
      </li>
      <li>
        <div class="left">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence-blue'); ?>">
              </use>
          </svg>
          <h4>I062E1</h4>
        </div>
        <div class="right">
          <p>Licenca za izgradnju elektroenergetskih instalacija srednjeg i visokog napona za trafostanice napona 110 i više KV</p>
        </div>
      </li>
      <li>
        <div class="left">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence-blue'); ?>">
              </use>
          </svg>
          <h4>I141E1</h4>
        </div>
        <div class="right">
          <p>Licenca za izgradnju elektroenergetskih instalacija srednjeg i visokog napona za javne železničke infrastrukture sa priključcima bez objekata</p>
        </div>
      </li>
    </ul>

    <h3>Partneri</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac ante sed tortor elementum luctus. Duis bibendum erat dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac ante sed tortor elementum luctus. Duis bibendum erat dolor.</p>
    
    <ul class="about__partners">
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
    </ul>
  </div>
</section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>
<section class="contact">
  <div class="container">
    <div class="left">
      <h1>Kontakt</h1>

      <div class="row">
        <p>ATM BG d.o.o</p>
        <p>Bulevar Mihajla Pupina 127</p>
        <p>11070 Novi Beograd</p>
      </div>

      <div class="row">
        <span>Email</span>
        <a href="mailto:office@atmbg.rs">office@atmbg.rs</a>
      </div>

      <div class="row">
        <span>Telefoni</span>
        <a href="tel:+381117112294">+381 (11) 711 22 94;</a>
        <a href="tel:+381117112307">+381 (11) 711 23 07;</a>
        <a href="tel:+381117118445">+381 (11) 711 84 45;</a>
        <a href="tel:+381117118771">+381 (11) 711 87 71;</a>
        <a href="tel:+381114155865">Fax: +381 (11) 415 58 65;</a>
      </div>

      <div class="row">
        <span>Izvršni direktor</span>
        <p>G-din Miodrag Petrović dipl. el. ing.</p>
      </div>

      <div class="row">
        <span>Generalni direktor</span>
        <p>G-din Milan Petrović</p>
      </div>
    </div>

    <div class="right">
      <h2>Možete nas kontaktirati i putem kontakt forme</h2>
      <form action="" class="contact__form">
        <input type="text" placeholder="Vaše ime">
        <input type="mail" placeholder="Email adresa">
        <input type="text" placeholder="Naslov poruke">
        <textarea name="" id="" placeholder="Poruka..."></textarea>

        <button type="submit" class="btn">Pošalji poruku</button>
      </form>
    </div>
  </div>

  <div id="map" class="contact__map"></div>
</section>
<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
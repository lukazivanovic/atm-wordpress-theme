<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => 'KONTAKTNA MREŽA'
    ]);
  ?>
  
  <section class="text-image">
    <div class="container">

      <!-- Row item -->
      <div class="row">
        <div class="left">
          <h4>REKONSTRUKCIJA PROJEKTOVANJE I ODRŽAVANJE SVIH OBJEKATA KONTAKTNE MREŽE</h4>
          <p>Projektovanje, isporuka opreme i izvođenje radova na ispravljačkim i elektrovučnim podstanicama, daljinskom upravljanju i kontaktnim mrežama za gradski saobraćaj (tramvaji i trolejbusi), železnice (25 kV), i industriju (JSS).</p>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/energetics-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->

    </div>
  </section>

  <section class="gallery">
    <div class="container">
      <?php
        get_partial('general/general-title', [
          'title' => 'Galerija referenci',
          'description' => 'Naš tim je stručan u gotovo svim oblastima energetike, tako da dajemo sugestije, savete i kompletna tehnička rešenja za dalekovode, trafo stanice, javnu rasvetu i dr.'
        ]);
      ?>

      <div class="gallery__wrapp">

        <a data-fancybox="gallery-1" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>BEOGRAD - PANČEVO</h4>
        </a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-1" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>


        <a data-fancybox="gallery-2" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>PROKOP</h4>
        </a>
        <a data-fancybox="gallery-2" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
        <a data-fancybox="gallery-2" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>

        <a data-fancybox="gallery-3" href="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" class="item">
          <img src="<?= buStatic('images/gallery/gallery-1.jpg'); ?>" alt="">
          <h4>VOJVODE STEPE</h4>
        </a>
        <a data-fancybox="gallery-3" href="<?= buStatic('images/energetics-cover.jpg'); ?>" style="display:none;"></a>
      </div>
    </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>
  
  <section class="cover cover--no-overlay cover--bottom-title" style="background: url('<?= buStatic('images/work-field.jpg'); ?>') no-repeat;">
    <div class="container">
      <h1>Elektrane i toplane</h1>
    </div>
  </section>

  <section class="work">
      <div class="container">
        <div class="left">

          <p class="intro-text">Along with those personal New Year resolutions, the start of a fresh year also offers the chance to change things up around your office or commercial property.</p>
          <p>A few simple tweaks and additions can go a long way to improving productivity and creativity among your staff. COS general manager Belinda Lyone has these simple tips for improving the amenity and the morale around your office as your team approaches 2018.</p>
          <p>Have you thought of providing breakfast or snacks for your employees? We know that a healthy, nutritious diet can help boost brain power, so why not provide the necessary snacks to help your employees work better in the new year?</p>
          <img src="<?= buStatic('images/work.jpg'); ?>" alt="">
          <h3>New year clean</h3>
          <p>What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>
          <h3>Sit stand desks</h3>
          <p>Sit to stand desks are a great way of providing choice to your employees. Giving your employees the choice of whether they want to sit or stand to work, will help workplace happiness, as well as productivity and creative thinking. What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>
          <p>What better way to start fresh than with a major clean of the office. Have your employees tidy up their desks, throwing away or filing clutter. You could get the carpets cleaned, windows washed and even tidy up that store room. Think about what you can clean to make the office look fresh come the new year.</p>
          <div class="share">
            <span>Podeli na mrežama</span>
            <ul class="share__list">
              <li>
                <a href="#">
                  <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-fb'); ?>">
                    </use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="#">
                  <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-tw'); ?>">
                    </use>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <aside class="right">
          <h2>Područje rada</h2>
          <ul class="right__nav">
            <li><a href="http://#"><span>01.</span>EPS Elektrodistribucija</a></li>
            <li><a href="http://#" class="is-active"><span>02.</span> Elektrane i toplane</a></li>
            <li><a href="http://#"><span>03.</span>Industrija</a></li>
            <li><a href="http://#"><span>04.</span> Gradski saobraćaj i železnica</a></li>
            <li><a href="http://#"><span>05.</span> Rekonstrukcija i održavanje energetskih objekata</a></li>
            <li><a href="http://#"><span>06.</span> Ostale delatnosti</a></li>
          </ul>
        </aside>
      </div>
  </section>

   <?php
    get_partial('general/projects-in-progress');
  ?>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>
  
  <section class="references">
      <div class="container">
        <h1>Reference</h1>

        <table>
          <thead>
            <tr>
              <th>Godina</th>
              <th colspan="2">Naručilac</th>
              <th>Investitor</th>
              <th>Objekat</th>
              <th colspan="2">Radovi</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>2016</td>
              <td colspan="2">Infrastruktura Železnice Srbije</td>
              <td>RŽD International	</td>
              <td>Resnik - Valjevo</td>
              <td colspan="2">Rekonstrukcija železničke kontaktne mreže</td>
            </tr>
            <tr>
              <td>2016</td>
              <td colspan="2">Infrastruktura Železnice Srbije</td>
              <td>RŽD International	</td>
              <td>Resnik - Valjevo</td>
              <td colspan="2">Rekonstrukcija železničke kontaktne mreže</td>
            </tr>
            <tr>
              <td>2016</td>
              <td colspan="2">Infrastruktura Železnice Srbije</td>
              <td>RŽD International	</td>
              <td>Resnik - Valjevo</td>
              <td colspan="2">Rekonstrukcija železničke kontaktne mreže</td>
            </tr>
            <tr>
              <td>2016</td>
              <td colspan="2">Infrastruktura Železnice Srbije</td>
              <td>RŽD International	</td>
              <td>Resnik - Valjevo</td>
              <td colspan="2">Rekonstrukcija železničke kontaktne mreže</td>
            </tr>
            <tr>
              <td>2016</td>
              <td colspan="2">Infrastruktura Železnice Srbije</td>
              <td>RŽD International	</td>
              <td>Resnik - Valjevo</td>
              <td colspan="2">Rekonstrukcija železničke kontaktne mreže</td>
            </tr>
          </tbody>
        </table>
      </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
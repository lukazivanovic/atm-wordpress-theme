<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

 <?php
    get_partial('general/cover', [
      'title' => 'Procesna merenja i automatizacija'
    ]);
  ?>
  
  <section class="text-image">
    <div class="container">

      <!-- Row item -->
      <div class="row">
        <h3>MERENJE TEMPERATURE</h3>

        <div class="left">
          <ul>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Termoelementi
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Otporni termometri
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Digitalni senzori temperature
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Pretvarači za ugradnju na šinu i u kućištu senzora:
              <ul>
                <li>Dvožični merni pretvarači napona i temperature</li>
                <li>Dvožični merni pretvarači otpora i temperature</li>
                <li>Četvorožični merni pretvarači napona i temperature</li>
                <li>Četvorožični merni pretvarači otpora i temperature</li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/image-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->


      <!-- Row item -->
      <div class="row">
        <h3>MERENJE TEMPERATURE</h3>

        <div class="left">
          <ul>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Termoelementi
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Otporni termometri
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Digitalni senzori temperature
            </li>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                  </use>
              </svg>
              Pretvarači za ugradnju na šinu i u kućištu senzora:
              <ul>
                <li>Dvožični merni pretvarači napona i temperature</li>
                <li>Dvožični merni pretvarači otpora i temperature</li>
                <li>Četvorožični merni pretvarači napona i temperature</li>
                <li>Četvorožični merni pretvarači otpora i temperature</li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/image-module.jpg'); ?>" alt="">
        </div>
      </div>
      <!-- Row item -->

      <div class="row">
        <div class="full">
          <h3>AUTOMATIZACIJA</h3>
          <ul>
            <li>Kompletan spektar usluga i najsavremeniju opremu</li>
            <li>Najsloženiju tehničku dokumentaciju za upravljanje automatizaciju i merenja</li>
            <li>Vršimo instalaciju, montažu opreme, povezivanje, kao i izradu kompleksnih SCADA aplikacija kojima ćete dobiti najsavremeniji i izuzetno pouzdan upravljačko-nadzorni sistem.</li>
            <li>Ispitivanje i puštanje u rad</li>
            <li>Servisne usluge</li>
            <li>Pomažemo u tehničkim rešenjima.</li>
          </ul>
        </div>
      </div>
      
    </div>
  </section>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
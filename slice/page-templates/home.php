<?php

require_once __DIR__ . '/../core/ini.php';

//WP HEADER
get_header();

//HOME HEADER
get_partial('layout/header');

?>

<section class="home__slider-holder">
  <div class="container">

    <div id="homeSlider" class="glide slider-home">
        
      <div class="glide__track" data-glide-el="track">
        
        <div class="glide__slides">
            <!-- Slide -->
            <div class="home__slide">

              <!-- 1125x570 -->
              <div class="media-wrapper" style="background: url('<?= buStatic('images/home/home-slider-large.png'); ?>') no-repeat;"></div>

              <div class="home__slide-box">
                <h4>Glavna železnička stanica Prokop - Beograd</h4>
                <div class="media-wrapper">
                  <img src="<?= buStatic('images/home/home-slider-small.jpg'); ?>" alt="485x416">
                </div>
              </div>
            </div>
            <!-- Slide -->

            <!-- Slide -->
            <div class="home__slide">

              <!-- 1125x570 -->
              <div class="media-wrapper" style="background: url('<?= buStatic('images/home/home-slider-large.png'); ?>') no-repeat;"></div>

              <div class="home__slide-box">
                <h4>Glavna železnička stanica Prokop - Beograd</h4>
                <div class="media-wrapper">
                  <img src="<?= buStatic('images/home/home-slider-small.jpg'); ?>" alt="485x416">
                </div>
              </div>
            </div>
            <!-- Slide -->

            <!-- Slide -->
            <div class="home__slide">

              <!-- 1125x570 -->
              <div class="media-wrapper" style="background: url('<?= buStatic('images/home/home-slider-large.png'); ?>') no-repeat;"></div>

              <div class="home__slide-box">
                <h4>Glavna železnička stanica Prokop - Beograd</h4>
                <div class="media-wrapper">
                  <img src="<?= buStatic('images/home/home-slider-small.jpg'); ?>" alt="485x416">
                </div>
              </div>
            </div>
            <!-- Slide -->
          </div>
        </div>

        <div class="home__slide-controls" data-glide-el="controls">
          <button data-glide-dir="<">
            <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
          </button>
          <button data-glide-dir=">">
            <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
          </button>
        </div>

        <div class="glide__bullets home__slide-info" data-glide-el="controls[nav]">
          <div class="glide__bullet" data-glide-dir="=0">
            <span class="index">01.</span>
            <p>Glavna železnička stanica Prokop - Beograd</p>
            <span>Beograd</span>
          </div>
          <div class="glide__bullet" data-glide-dir="=1">
            <span class="index">02.</span>
            <p>Glavna železnička stanica Prokop - Beograd</p>
            <span>Beograd</span>
          </div>
          <div class="glide__bullet" data-glide-dir="=2">
            <span class="index">03.</span>
            <p>Glavna železnička stanica Prokop - Beograd</p>
            <span>Beograd</span>
          </div>
        </div>

    </div>

  </div>
</section>

 <?php
    get_partial('general/projects-in-progress');
    get_partial('home/projects');
    get_partial('home/licence');
    get_partial('home/partners');
    get_partial('home/certificate');
  ?>

<?php

get_partial('layout/footer', [
        'footerClass' => 'footer--home',
]);

//WP FOOTER
get_footer();
<section class="projects">

    <!-- row -->
    <div class="row">
      <div class="container">
        <div class="left">
          <img src="<?= buStatic('images/home/eps-elektrodistribucija.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>EPS Elektrodistribucija</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Trafostanice</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Visokonaponske, srednjenaponske i niskonaponske mreže</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/home/elektrane-i-toplane.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>Elektrane i toplane</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Automatizacija, upravljanje i nadzor</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Procesna merenja</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->

    <!-- row 2. -->
    <div class="row row--two">
      <div class="container">
        <div class="left">
          <img src="<?= buStatic('images/home/industrija.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>Industrija</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Kompletna energetika</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Razvod niskog i srednjeg napona</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Automatizacija u procesu proizvodnje i optimizacija sa procesnim merenjima</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/home/gradski-saobracaj.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>Gradski saobraćaj i železnice</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Izgradnja kontaktnih mreža</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Ispravljačke i elektrovučne podstanice</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- row 2. -->
    
    <!-- row 3. -->
    <div class="row row--two row--three">
      <div class="container">
        <div class="left">
          <img src="<?= buStatic('images/home/rekonstrukcija.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>Rekonstrukcija i održavanje energetskih objekata</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Trafostanice</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Dalekovodi</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Kontaktne mreže</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
        <div class="right">
          <img src="<?= buStatic('images/home/ostale-deltanosti.jpg'); ?>" alt="">
          <div class="projects__item-info">
            <h4>Ostale delatnosti</h4>
            <ul>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Energetska efikasnost</span>
              </li>
              <li>
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-flash'); ?>">
                    </use>
                </svg>
                <span>Kompezacija</span>
              </li>
            </ul>
            <a href="#" class="btn btn--arrow btn--outline">
              <span>Pročitaj više</span>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-angle-right'); ?>">
                  </use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- row 3. -->


</section>
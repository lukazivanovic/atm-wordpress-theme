<section class="licence" style="background: url('<?= buStatic('images/home/licence.jpg'); ?>') no-repeat;">
  <div class="container">
    <?php
      get_partial('general/general-title', [
        'title' => 'Licence',
        'description' => 'Posedovanje licence za projektovanje i izvođenje radova, omogućava učešće u najsloženijim projektima, i daje garanciju da će svi poslovi koji budu ugovoreni, biti izvršeni na tehnički najefikasniji način, po povoljnoj ceni i predviđenom roku.'
      ]);
    ?>

    <div class="row">
      <div class="col">
        <svg class="icon">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence'); ?>">
            </use>
        </svg>
        <h4>P141E1</h4>
        <p>Licenca za projektovanje elektroenergetskih instalacija srednjeg i visokog napona za javne železničke infrastrukture sa priključcima</p>
      </div>

      <div class="col">
        <svg class="icon">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence'); ?>">
            </use>
        </svg>
        <h4>I062E1</h4>
        <p>Licenca za projektovanje elektroenergetskih instalacija srednjeg i visokog napona za javne železničke infrastrukture sa priključcima</p>
      </div>

      <div class="col">
        <svg class="icon">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-licence'); ?>">
            </use>
        </svg>
        <h4>I141E1</h4>
        <p>Licenca za projektovanje elektroenergetskih instalacija srednjeg i visokog napona za javne železničke infrastrukture sa priključcima</p>
      </div>
    </div>
    
  </div>
</section>
<section class="certificates">
  <div class="container">
    <?php
        get_partial('general/general-title', [
          'title' => 'Sertifikati',
          'description' => 'Posedovanje licence za projektovanje i izvođenje radova, omogućava učešće u najsloženijim projektima, i daje garanciju da će svi poslovi koji budu ugovoreni, biti izvršeni na tehnički najefikasniji način, po povoljnoj ceni i predviđenom roku.'
        ]);
      ?>
  </div>

  <div class="certificates__cols">
    <div class="col">
      <svg class="icon">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
          </use>
      </svg>
      <span>ISO 9001</span>
    </div>
    <div class="col">
      <svg class="icon">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
          </use>
      </svg>
      <span>ISO 14001</span>
    </div>
    <div class="col">
      <svg class="icon">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= buStatic('ui/symbol-defs.svg#icon-cert'); ?>">
          </use>
      </svg>
      <span>OHSAS 18001</span>
    </div>
  </div>

  <div class="certificates__info">
      <div class="row">
          <div class="item item--blue item--blue-wide">
            <div>
              <strong>45</strong>
              <span>km razvučenih naponskih kablova</span>
            </div>
          </div>

          <div class="item item--small">
            <div>
              <strong></strong>
              <span></span>
            </div>
          </div>
          
          <div class="item item--image" style="background: url('<?= buStatic('images/home/cert-image.jpg'); ?>');">
            <div>
              <strong>30</strong>
              <p>instaliranih trafo stanica</p>
            </div>
          </div>
      </div>
      <div class="row">
          <div class="item item--blue-wide">
            <div>
              <strong></strong>
              <span></span>
            </div>
          </div>

        <div class="item item--blue item--small">
          <div>
            <strong>300</strong>
            <span>kvadrata postavljenih solarnih panela</span>
          </div>
        </div>

          <div class="item item--solid">
            <div>
              <strong>Lorem ipsum</strong>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac ante sed tortor elementum luctus. Duis bibendum erat dolor.</p>
            </div>
          </div>
      </div>
  </div>
</section>
<section class="partners">
  <div class="container">
    <?php
      get_partial('general/general-title', [
        'title' => 'Partneri',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac ante sed tortor elementum luctus. Duis bibendum erat dolor.'
      ]);
    ?>
  </div>

  <div class="container container--slider">
  <div id="partnersSlider" class="glide slider-partners">
        
        <div class="glide__track" data-glide-el="track">
          
          <div class="glide__slides">
              <!-- Slide -->
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                    </ul>
              </div>
              <!-- Slide -->
              <!-- Slide -->
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                    </ul>
              </div>
              <!-- Slide -->
              <!-- Slide -->
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                      <li><img src="<?= buStatic('images/home/partner.png'); ?>" alt=""></li>
                    </ul>
              </div>
              <!-- Slide -->
            </div>
          </div>
  
          <div class="glide__bullets partners__dots" data-glide-el="controls[nav]">
            <div class="glide__bullet" data-glide-dir="=0">
              
            </div>
            <div class="glide__bullet" data-glide-dir="=1">
              
            </div>
            <div class="glide__bullet" data-glide-dir="=2">
              
            </div>
          </div>
  
      </div>
  </div>
</section>
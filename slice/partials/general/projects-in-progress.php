<section class="project-articles">
  <div class="container">
    <?php
      get_partial('general/general-title', [
        'title' => 'Projekti u toku',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac ante sed tortor elementum luctus. Duis bibendum erat dolor.'
      ]);
    ?>

    <div id="slider" class="glide slider-articles">
      <div class="glide__track" data-glide-el="track">
        <div class="glide__slides">

          <article class="slider-articles__slide">
            <img src="https://via.placeholder.com/360x230" alt="">
            <h3>Enhance Your Life By Having A Sense Of Purpose</h3>
            <span class="location">Niš</span>
            <a href="#" class="btn btn--outline">
              <span>Pročitaj više</span>
            </a>
          </article>

          <article class="slider-articles__slide">
            <img src="https://via.placeholder.com/360x230" alt="">
            <h3>How Great Is The Strength Of Your Belief</h3>
            <span class="location">Niš</span>
            <a href="#" class="btn btn--outline">
              <span>Pročitaj više</span>
            </a>
          </article>

          <article class="slider-articles__slide">
            <img src="https://via.placeholder.com/360x230" alt="">
            <h3>Enhance Your Life By Having A Sense Of Purpose</h3>
            <span class="location">Niš</span>
            <a href="#" class="btn btn--outline">
              <span>Pročitaj više</span>
            </a>
          </article>

          <article class="slider-articles__slide">
            <img src="https://via.placeholder.com/360x230" alt="">
            <h3>Enhance Your Life By Having A Sense Of Purpose</h3>
            <span class="location">Niš</span>
            <a href="#" class="btn btn--outline">
              <span>Pročitaj više</span>
            </a>
          </article>

        </div>
      </div>

      <div class="slider-articles__controls" data-glide-el="controls">
        <button data-glide-dir="<">
          <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
        </button>
        <button data-glide-dir=">">
          <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
        </button>
      </div>
      
    </div>
  </div>
</section>
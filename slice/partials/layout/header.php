<header class="header">

    <div class="header__top">
      <div class="container">
        <ul class="header__top-nav">
          <li><a href="http://#">Početna</a></li>
          <li><a href="http://#">O nama</a></li>
          <li><a href="http://#">Kontakt</a></li>
        </ul>
        <a href="#">
          <img src="https://via.placeholder.com/30x20" alt="">
        </a>
      </div>
    </div>

    <div class="header__bottom">
      <div class="container">
      <a href="#" class="header__logo">
        <img src="<?= buStatic('images/logo.png'); ?>" alt="90x80">
      </a>

      <nav class="header__nav">
        <ul>
          <li><a href="http://#">Energetika</a></li>
          <li><a href="http://#">KONTAKTNA MREŽA</a></li>
          <li><a href="http://#">PROCESNA MERENJA I AUTOMATIZACIJA</a></li>
          <li><a href="http://#">OSTALE DELATNOSTI</a></li>
          <li><a href="http://#">REFERENCE</a></li>
        </ul>
      </nav></div>
    </div>

</header>
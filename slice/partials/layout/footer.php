<!-- FOOTER -->
<footer class="footer <?=$footerClass?>">
  <div class="container">
    
    <div class="row">
      <div class="col">
        <ul>
          <li>ATM BG D.O.O.</li>
          <li>Bulvear Mihajla Pupina 127 11070 Novi Beograd, Srbija</li>
          <li>Email: <a href="mailto:office@atmbg.rs">office@atmbg.rs</a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
          <li><a href="tel:+381117112294">+381 (11) 711 22 94</a></li>
          <li><a href="tel:+381117112307">+381 (11) 711 23 07</a></li>
          <li><a href="tel:+381117118445">+381 (11) 711 84 45</a></li>
          <li><a href="tel:+381117118771">+381 (11) 711 87 71</a></li>
          <li>Fax: <a href="tel:+381114155865">+381 (11) 415 58 65</a></li>
        </ul>
      </div>
    </div>

    <ul class="footer__nav">
      <li><a href="http://#">Početna</a></li>
      <li><a href="http://#">Energetika</a></li>
      <li><a href="http://#">Kontaktna mreža</a></li>
      <li><a href="http://#">Procesna merenja i automatizacija</a></li>
      <li><a href="http://#">Ostale delatnosti</a></li>
      <li><a href="http://#">Reference</a></li>
      <li><a href="http://#">Kontakt</a></li>
    </ul>
    
  </div>
</footer>
<!-- //FOOTER -->
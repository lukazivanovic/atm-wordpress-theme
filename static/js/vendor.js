// Add modules here to include them in dist/vendor.js file for improved code splitting

// IMPORTANT: babel-polyfill must be imported, but only once, so leave it imported only in this file
// *********************
import 'babel-polyfill';
// *********************
import 'jquery';
import 'svgxuse';
import 'slick-carousel/slick/slick.js';
import 'vendor/jquery.fancybox.js';

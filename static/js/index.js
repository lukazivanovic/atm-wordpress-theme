import 'style/style.scss';
import HomePage from 'pages/HomePage';
import 'vendor/jquery.fancybox.js';

$(document).ready(() => {
    const homePage = new HomePage();
    homePage.init();
    
    function initMap() {
      var atm = {lat: 44.807872, lng: 20.388690};
      var map = new google.maps.Map(
          document.getElementById('map'), {zoom: 16, center: atm});
      var marker = new google.maps.Marker({position: atm, map: map});
    }
    
    if($('#map').length){
      initMap();
    }
});
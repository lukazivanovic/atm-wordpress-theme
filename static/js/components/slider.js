import  Glide from '@glidejs/glide/dist/glide.min.js';

const Slider = class Slider {

    constructor() {

    }

    init() {
        this.slider();
        this.partnersSlider();
    }

    slider() {

        if($('#slider').length){
            let glide = new Glide('#slider', {
                bound: true,
                startAt: 0,
                perView: 3,
                gap: 40,
                peek: {
                  before: 40,
                  after: 40
                },
                breakpoints: {
                    900: {
                        gap: 40,
                        peek: {
                            before: 40,
                            after: 80
                        }
                    }
                }
            });
            glide.mount();
        }

        if($('#homeSlider').length){
          let glide = new Glide('#homeSlider', {
              bound: true,
              startAt: 0,
              perView: 1,
              gap: 0,
              breakpoints: {
                  900: {
                      gap: 40,
                      peek: {
                          before: 40,
                          after: 80
                      }
                  }
              }
          });
          glide.mount();
        }
    }

    partnersSlider(){
      if($('#partnersSlider').length){
          let glide = new Glide('#partnersSlider', {
              bound: true,
              startAt: 0,
              perView: 1,
              gap: 0,
          });
          glide.mount();
      }
    }
};

export {Slider}

import {Slider} from "../components/slider";

export default class HomePage {
    constructor() {

    }

    init() {
      const slider = new Slider();
      slider.init();
    }
}
<?php

define('INCLUDE_PATH', get_template_directory() . '/inc/');
define('TEMPLATE_PATH', get_template_directory() . '/');
define('INCLUDE_URL', get_template_directory_uri());
define('FS_METHOD', 'direct');

if (! file_exists(__DIR__ . '/vendor/autoload.php')) {
    add_action('admin_notices', function () {
        ?>
        <div class="notice notice-error">
            <h2>Missing <i>vendor/autoloader.php</i></h2>
            <p>
                <strong>
                    You are missing composer autoload. Please run <i>composer dump</i> in root of your project.
                </strong>
            </p>
        </div>
        <?php
    });

    return;
}
/**
 * If this command fails try to run "composer dump" in the theme root directory
 */
require_once __DIR__ . '/vendor/autoload.php';

use app\core\CustomPostTypes;

$customPostTypes = new CustomPostTypes();
$customPostTypes->register();

/**
 * Base url convertion method.
 * @param $url
 * @return string
 */
function bu($url)
{
    $clean = trim($url);

    return INCLUDE_URL . "/static/" . $clean;
}

function register_my_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
}

add_action('init', 'register_my_menu');

function register_my_menus()
{
    register_nav_menus(['header-menu' => __('Header Menu')]);
}

add_action('init', 'register_my_menus');

/**
 * helper for getting options field from acf
 * @param $field
 * @return bool|mixed|null
 */
function get_field_option($field)
{
    return get_field($field, 'options');
}

add_filter('acf/settings/load_json', 'acf_json_load_point');
function acf_json_load_point($paths)
{
    $paths[] = get_stylesheet_directory() . '/acf-json/en';

    return $paths;
}

function create_menu(string $menuName)
{
    $pages = wp_get_nav_menu_items($menuName);
    if ($pages) {
        foreach ($pages as $item) {
            ?>
            <li>
                <a href="<?= $item->url ?>"><?= $item->title ?></a>
            </li>
            <?php
        }
    }
}

function create_menu_work_areas(string $menuName){
    $pages = wp_get_nav_menu_items($menuName);
    if($pages){
        foreach($pages as $key=>$item){
            ?>
            <li><a href="<?= $item->url ?>" <?= $item->title == get_the_title() ? 'class="is-active"' : "" ?>><span>0<?= $key+1 ?>.</span><?= $item->title ?></a></li>
            <?php
        }
    }
}
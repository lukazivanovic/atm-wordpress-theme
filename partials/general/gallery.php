<div class="gallery__wrapp">
        
    <?php 
        if(is_array($gallery_slider)&&(!empty($gallery_slider))){
        foreach($gallery_slider as $first_key=>$gallery_slider_item){
    ?>

    <a data-fancybox="gallery-<?=$first_key?>" href="<?= $gallery_slider_item['main_image']['url'] ?>" class="item">
        <img src="<?= $gallery_slider_item['main_image']['url'] ?>" alt="">
        <h4><?=$gallery_slider_item['title']?></h4>
    </a>
    <?php foreach($gallery_slider_item['images'] as $key=>$gallery_slider_images){ ?>
    <a data-fancybox="gallery-<?=$key?>" href="<?= $gallery_slider_images['image']['url'] ?>" style="display:none;"></a>
    <?php }}} ?>

</div>
<section class="cover" style="background: url('<?= $image ?>') no-repeat;">
  <div class="container">
    <h1><?= $title ?></h1>
  </div>
</section>
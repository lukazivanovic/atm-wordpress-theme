<?php
use app\common\ACFDataProvider;
use app\helpers\PostHelper;

$acf_data_instance = ACFDataProvider::getInstance()->setPrefix('project_');

$title = $acf_data_instance->getOptionField('title');
$into = $acf_data_instance->getOptionField('intro');
$projects = $acf_data_instance->getOptionField('items');

?>
<section class="project-articles">
  <div class="container">
    <?php
      get_partial('general/general-title', [
        'title' => $title,
        'description' => $into
      ]);
      if (is_array($projects) && !empty($projects)) {
    ?>
    <div id="slider" class="glide slider-articles">
      <div class="glide__track" data-glide-el="track">
        <div class="glide__slides">
            <?php foreach ($projects as $project) {
                $postHelper = new PostHelper($project['item']);
                ?>
                <article class="slider-articles__slide">
                    <img src="<?= $postHelper->getFeaturedImage() ?>" alt="">
                    <h3><?= $postHelper->getTitle() ?></h3>
                    <span class="location"><?= $acf_data_instance->getField('location'); 
                    //, $project['item'], false ?></span>
                    <a href="<?= $postHelper->getPermalink() ?>" class="btn btn--outline">
                        <span><?= __('Pročitaj više', 'atm'); ?></span>
                    </a>
                </article>

                <?php
            }
            ?>
        </div>
      </div>
    <?php } ?>

      <div class="slider-articles__controls" data-glide-el="controls">
        <button data-glide-dir="<">
          <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
        </button>
        <button data-glide-dir=">">
          <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
        </button>
      </div>
      
    </div>
  </div>
</section>
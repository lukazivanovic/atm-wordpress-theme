<?php /** Template Name: About Template*/

use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance()->setPrefix('certificate_-_');
$certificate_title = $acf_instance->getField('title');
$certificate_info = $acf_instance->getField('info');
$certificate_items = $acf_instance->getField('items');

?>

<h3><?=$certificate_title?></h3>
    <p><?=$certificate_info?></p>
    
    <div class="certificates__cols">
    <?php  
        if(is_array($certificate_items) && !empty($certificate_items)){
            foreach($certificate_items as $certificate_item){
    ?>
        <div class="col">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-cert'); ?>">
              </use>
          </svg>
          <span><?= $certificate_item['title'] ?></span>
        </div>
        <?php }} ?>
    </div>
<?php /** Template Name: About Template*/

use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance()->setPrefix('about_-_');
$about_image = $acf_instance->getField('cover_image');
$about_text = $acf_instance->getField('text');

?>
<section class="cover cover--no-overlay cover--inner" style="background: url('<?= $about_image['url'] ?>') no-repeat;"></section>

<section class="about">
  <div class="container">
    
    <?= $about_text ?>
    
    <?php 
    $licence_items = $acf_instance->getField('licence_-_items', false, false);
    ?>
    <ul class="about__licence">
    <?php if(is_array($licence_items) && !empty($licence_items)){
        foreach($licence_items as $licence_item){ ?>
      <li>
        <div class="left">
          <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-licence-blue'); ?>">
              </use>
          </svg>
          <h4><?=$licence_item['title']?></h4>
        </div>

        <div class="right">
          <p><?=$licence_item['description']?></p>
        </div>
      </li>
          <?php }} ?>
    </ul>

    

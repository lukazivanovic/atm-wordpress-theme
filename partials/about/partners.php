<?php /** Template Name: About Template*/

use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance()->setPrefix('partners_-_');
$partners_info = $acf_instance->getField('info');
$partners_logos = $acf_instance->getField('logos');

echo $partners_info;
?>

<ul class="about__partners">
<?php if(is_array($partners_logos) && !empty($partners_logos)){
        foreach($partners_logos as $partners_logo){ ?>
    <li><img src="<?= $partners_logo['image']['url'] ?>" alt=""></li>
    <?php }} ?>
</ul>

</div>
</section>

<!-- Row item -->
<div class="row">
        <h3><?= $item['title'] ?></h3>

        <div class="left">
          <ul>
          <?php 
        if(is_array($item['list_icon']) && !empty($item['list_icon'])){
          foreach($item['list_icon'] as $list_icon){ ?>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-'.$item['icon']['title']); ?>">
                  </use>
              </svg>
              <?=$list_icon['list_text']?>
            </li>
            <?php }} ?>
            <?php 
            if(is_array($item['list_no_icon']) && !empty($item['list_no_icon'])){
            foreach($item['list_no_icon'] as $list_no_icon){ ?>
            <li>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-'.$item['icon']['title']); ?>">
                  </use>
              </svg>
              <?=$list_no_icon['list_icon_text']?>
              <ul>
              <?php 
              if(is_array($list_no_icon['list']) && !empty($list_no_icon['list'])){
              foreach($list_no_icon['list'] as $list){ ?>
                <li><?=$list['list_text']?></li>
              <?php }} ?>
              </ul>
            </li>
              <?php }} ?>
          </ul>
        </div>
        <div class="right">
          <img src="<?= $item['image']['url'] ?>" alt="">
        </div>
      </div>
      <!-- Row item -->
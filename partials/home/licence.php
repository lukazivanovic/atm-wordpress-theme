<?php
use app\common\ACFDataProvider;
$acf_instance = ACFDataProvider::getInstance()->setPrefix('license_-_');

$license_lists = $acf_instance->getField('list');
?>

<section class="licence" style="background: url('<?= $acf_instance->getField('cover_image')['url']; ?>') no-repeat;">
    <div class="container">
        <?php
        get_partial('general/general-title', [
            'title' => $acf_instance->getField('title'),
            'description' => $acf_instance->getField('content')
        ]);
        ?>

        <div class="row">
        <?php 
            if(is_array($license_lists) && !empty($license_lists)){
                foreach($license_lists as $license_list){
                    ?>
               <div class="col">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#'.$license_list['logo']); ?>">
                    </use>
                </svg>
                <h4><?= $license_list['title'] ?></h4>
                <p><?= $license_list['content'] ?></p>
            </div>
               <?php }
            }
        ?>
        </div>

    </div>
</section>
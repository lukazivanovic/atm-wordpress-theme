<?php

use app\common\ACFDataProvider;

$acf_data_instance = ACFDataProvider::getInstance()->setPrefix('work_-_');

$all_projects = $acf_data_instance->getField('areas');
$projects = array_chunk($all_projects, 2);
?>

<section class="projects">
    <?php if (is_array($projects) && !empty($projects)) {
        foreach ($projects as $key => $project) {
            $class = 'row ';

            if ($key == 1) {
                $class .= 'row--two';
            } else if ($key == 2) {
                $class .= 'row--two row--three';
            }

            $content_first = $project[0]['content'];
            $content_second = $project[1]['content'];

            ?>
            <div class="<?= $class ?>">
                <div class="container">
                    <div class="left">
                        <img src="<?= $project[0]['image']['url']; ?>" alt="">
                        <div class="projects__item-info">
                            <h4><?= $project[0]['title'] ?></h4>
                            <?php if (is_array($content_first)) { ?>
                                <ul>
                                    <?php foreach ($content_first as $content) { ?>
                                        <li>
                                            <svg class="icon">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     xlink:href="<?= bu('ui/symbol-defs.svg#icon-flash'); ?>">
                                                </use>
                                            </svg>
                                            <span><?= $content['text'] ?></span>
                                        </li>
                                        <?php
                                    }
                                ?>
                                </ul>
                                <?php
                            }
                            ?>
                            <a href="<?= $project[0]['link'] ?>" class="btn btn--arrow btn--outline">
                                <span><?= __('Pročitaj više', 'atm'); ?></span>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xlink:href="<?= bu('ui/symbol-defs.svg#icon-angle-right'); ?>">
                                    </use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="right">
                        <img src="<?= $project[1]['image']['url']; ?>" alt="">
                        <div class="projects__item-info">
                            <h4><?= $project[1]['title'] ?></h4>
                            <?php if (is_array($content_second)) { ?>
                                <ul>
                                    <?php foreach ($content_second as $content) { ?>
                                        <li>
                                            <svg class="icon">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     xlink:href="<?= bu('ui/symbol-defs.svg#icon-flash'); ?>">
                                                </use>
                                            </svg>
                                            <span><?= $content['text'] ?></span>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                            <a href="<?= $project[1]['link'] ?>" class="btn btn--arrow btn--outline">
                                <span>Pročitaj više</span>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xlink:href="<?= bu('ui/symbol-defs.svg#icon-angle-right'); ?>">
                                    </use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</section>
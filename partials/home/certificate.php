<?php
use app\common\ACFDataProvider;
use app\helpers\PostHelper;

$acf_data_instance = ACFDataProvider::getInstance()->setPrefix('certificate_-_');

$certificate_title = $acf_data_instance->getField('title');
$certificate_content = $acf_data_instance->getField('content');
$certificate_list = $acf_data_instance->getField('list');

?>

<section class="certificates">
  <div class="container">
    <?php
        get_partial('general/general-title', [
          'title' => $certificate_title,
          'description' => $certificate_content
        ]);
      ?>
  </div>

<?php if(is_array($certificate_list) && !empty($certificate_list)){ ?>
  <div class="certificates__cols">
  <?php foreach($certificate_list as $certificate_list_item){ ?>
    <div class="col">
      <svg class="icon">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= bu('ui/symbol-defs.svg#icon-cert'); ?>">
          </use>
      </svg>
      <span><?= $certificate_list_item['title'] ?></span>
    </div>
    <?php } ?>
  </div>
<?php } ?>

  
</section>
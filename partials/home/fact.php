<?php
use app\common\ACFDataProvider;
use app\helpers\PostHelper;

$acf_data_instance = ACFDataProvider::getInstance()->setPrefix('fact_-_');

$fact_above_item_first_title = $acf_data_instance->getField('above_item_first_title');
$fact_above_item_first_data = $acf_data_instance->getField('above_item_first_data');
$fact_above_item_first_image = $acf_data_instance->getField('above_item_first_image');
$fact_above_item_second_title = $acf_data_instance->getField('above_item_second_title');
$fact_above_item_second_data = $acf_data_instance->getField('above_item_second_data');
$fact_under_item_first_title = $acf_data_instance->getField('under_item_first_title');
$fact_under_item_first_data = $acf_data_instance->getField('under_item_first_data');
$fact_under_item_second_title = $acf_data_instance->getField('under_item_second_title');
$fact_under_item_second_data = $acf_data_instance->getField('under_item_second_data');

?>
<div class="certificates__info">
      <div class="row">
          <div class="item item--blue item--blue-wide">
            <div>
              <strong><?= $fact_above_item_first_data ?></strong>
              <span><?= $fact_above_item_first_title ?></span>
            </div>
          </div>

          <div class="item item--small">
            <div>
              <strong></strong>
              <span></span>
            </div>
          </div>
          
          <div class="item item--image" style="background: url('<?= $fact_above_item_first_image['url'] ?>');">
            <div>
              <strong><?= $fact_above_item_second_data ?></strong>
              <p><?= $fact_above_item_second_title ?></p>
            </div>
          </div>
      </div>
      <div class="row">
          <div class="item item--blue-wide">
            <div>
              <strong></strong>
              <span></span>
            </div>
          </div>

        <div class="item item--blue item--small">
          <div>
            <strong><?= $fact_under_item_first_data ?></strong>
            <span><?= $fact_under_item_first_title ?></span>
          </div>
        </div>

          <div class="item item--solid">
            <div>
              <strong><?= $fact_under_item_second_data ?></strong>
              <p><?= $fact_under_item_second_title ?></p>
            </div>
          </div>
      </div>
  </div>
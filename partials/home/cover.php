<?php
use app\common\ACFDataProvider;

$acf_data_instance = ACFDataProvider::getInstance()->setPrefix('cover_-_');
$slider = $acf_data_instance->getField('slider');

?>
<section class="home__slider-holder">
    <div class="container">

        <div id="homeSlider" class="glide slider-home">

            <div class="glide__track" data-glide-el="track">

                <div class="glide__slides">
                    <?php foreach ($slider as $item) {
                        ?>
                        <div class="home__slide">
                            <div class="media-wrapper"
                                 style="background: url('<?= $item['cover_-_image']['url']; ?>') no-repeat;"></div>

                            <div class="home__slide-box">
                                <h4><?= $item['cover_-_title'] . ' - ' . $item['cover_-_location']?></h4>
                                <div class="media-wrapper">
                                    <img src="<?= $item['cover_-_image_small']['url']; ?>" alt="485x416">
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="home__slide-controls" data-glide-el="controls">
                <button data-glide-dir="<">
                    <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
                </button>
                <button data-glide-dir=">">
                    <svg width="22" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M1488.5 650.8h-20" id="a"/><path d="M1475.5 657.1l-7-6.3 7-6.3" id="b"/></defs><g transform="translate(-1466 -643)"><use xlink:href="#a" fill-opacity="0" fill="#fff" stroke-miterlimit="50" stroke-width="2"/></g><g transform="translate(-1466 -643)"><use xlink:href="#b" fill-opacity="0" fill="#fff" stroke-linecap="square" stroke-miterlimit="50" stroke-width="2"/></g></svg>
                </button>
            </div>

            <div class="glide__bullets home__slide-info" data-glide-el="controls[nav]">
                <?php foreach ($slider as $key => $item) { ?>
                <div class="glide__bullet" data-glide-dir="=<?= $key ?>">
                    <span class="index"><?= ++$key . '.' ?></span>
                    <p><?= $item['cover_-_title'] . ' - ' . $item['cover_-_location'] ?></p>
                    <span><?= $item['cover_-_location'] ?></span>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</section>
<?php
use app\common\ACFDataProvider;

$acf_instance = ACFDataProvider::getInstance()->setPrefix('partner_-_');

$partners_list = $acf_instance->getField('list');
$partners_list_second = $acf_instance->getField('list_second');
$partners_list_third = $acf_instance->getField('list_third');

$title = $acf_instance->getField('title');
$content = $acf_instance->getField('content');


?>

<section class="partners">
  <div class="container">
    <?php
      get_partial('general/general-title', [
        'title' => $title,
        'description' => $content
      ]);
    ?>
  </div>

  <div class="container container--slider">
  <div id="partnersSlider" class="glide slider-partners">
        
        <div class="glide__track" data-glide-el="track">
          
          <div class="glide__slides">
              <!-- Slide -->
              <?php if(is_array($partners_list) && !empty($partners_list)){ ?>
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                    <?php foreach($partners_list as $partners_list_item){ ?>
                      <li><img src="<?= $partners_list_item['logo']['url'] ?>" alt=""></li>
                    <?php } ?>
                    </ul>
              </div>
              <?php } ?>
              <!-- Slide -->
              <!-- Slide -->
              <?php if(is_array($partners_list_second) && !empty($partners_list_second)){ ?>
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                    <?php foreach($partners_list_second as $partners_list_second_item){  ?>
                      <li><img src="<?= $partners_list_second_item['logo']['url'] ?>" alt=""></li>
                      <?php } ?>
                    </ul>
              </div>
              <?php } ?>
              <!-- Slide -->
              <!-- Slide -->
              <?php if(is_array($partners_list_third) && !empty($partners_list_third)){ ?>
              <div class="partners__slide">
                    <ul class="partners__slide-list">
                    <?php foreach($partners_list_third as $partners_list_third_item){ ?>
                      <li><img src="<?= $partners_list_third_item['logo']['url'] ?>" alt=""></li>
                      <?php } ?>
                    </ul>
              </div>
              <?php } ?>
              <!-- Slide -->
            </div>
          </div>
  
          <div class="glide__bullets partners__dots" data-glide-el="controls[nav]">
            <div class="glide__bullet" data-glide-dir="=0">
              
            </div>
            <div class="glide__bullet" data-glide-dir="=1">
              
            </div>
            <div class="glide__bullet" data-glide-dir="=2">
              
            </div>
          </div>
  
      </div>
  </div>
</section>
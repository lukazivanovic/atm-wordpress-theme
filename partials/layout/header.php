<header class="header">

    <div class="header__top">
      <div class="container">
        <ul class="header__top-nav">
            <?php create_menu('First Header Menu') ?>
        </ul>
        <a href="#">
          <img src="https://via.placeholder.com/30x20" alt="">
        </a>
      </div>
    </div>

    <div class="header__bottom">
      <div class="container">
      <a href="<?= get_home_url() ?>" class="header__logo">
        <img src="<?= bu('images/logo.png'); ?>" alt="90x80">
      </a>

      <nav class="header__nav">
        <ul>
          <?php create_menu('Second Header Menu') ?>
        </ul>
      </nav></div>
    </div>

</header>
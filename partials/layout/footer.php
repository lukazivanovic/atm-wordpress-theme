<?php
use app\common\ACFDataProvider;
use app\helpers\PostHelper;

$acf_instance = ACFDataProvider::getInstance()->setPrefix(false);

$company_name = $acf_instance->getOptionField('company_name');
$address = $acf_instance->getOptionField('address');
$email = $acf_instance->getOptionField('email');
$telephone = $acf_instance->getOptionField('telephone');
$fax = $acf_instance->getOptionField('fax');
?>
<!-- FOOTER -->
<footer class="footer <?=$footerClass?>">
  <div class="container">
    
    <div class="row">
      <div class="col">
        <ul>
          <li><?=$company_name?></li>
          <li><?=$address?></li>
          <li>Email: <a href="mailto:<?=$email?>"><?=$email?></a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
        <?php 
          if(is_array($telephone) && !empty($telephone)){
            foreach($telephone as $tel){
        ?>
          <li><a href="tel:<?=preg_replace('/[()\s]/','', $tel['number'])?>"><?=$tel['number']?></a></li>
          <?php }} ?>
          <li>Fax: <a href="tel:<?=preg_replace('/[()\s]/','', $fax)?>"><?=$fax?></a></li>
        </ul>
      </div>
    </div>

    <ul class="footer__nav">
      <?php create_menu('Footer Menu'); ?>
    </ul>
    
  </div>
</footer>
<!-- //FOOTER -->
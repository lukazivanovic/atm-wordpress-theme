<?php
add_theme_support( 'post-thumbnails' );

define('API_BASE_PATH', 'api/v1');

define('DATE_FORMAT', 'm/d/Y');
<?php

//Don't add image size with name 'full'. ACF uses this size for the original image

/**
 *
 *
 * Adding custom size - add_image_size( 'header', 1920, 800, true );
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * Adding custom width and auto height - add_image_size( 'header', 600, 9999, false );
 * Set wished auto dimension to some big value - crop should be false
 *
 */


//add_image_size( 'small-square', 205, 205, true );
//add_image_size( 'big-square', 420, 420, true );
//add_image_size( 'small-rectangle', 420, 205, true );
//add_image_size( 'big-rectangle', 850, 420, true );
//add_image_size( 'vertical-rectangle', 205, 420, true );

//add_image_size( 'cover-image', 1280, 720, true );

//add_image_size( 'featured-image', 860, 515, true );

//add_image_size( 'word-list-image', 415, 230, true );

//add_image_size( 'latest-work', 390, 220, true );